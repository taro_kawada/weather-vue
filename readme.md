# Weather App with Vue.js

- Built with bootstrap and vue.
- First page shows cards of Tokyo, Yokohama, Kyoto, Osaka, Sapporo, Nagoya with the option to see more information.
- Result page shows a slider of temperature, condition, date and time.
- Search allows user to search more than just Tokyo, Yokohama, Kyoto, Osaka, Sapporo and Nagoya.
- If search result is undefined, an error message will inform the user.

App is hosted here https://determined-ritchie-377f73.netlify.com/.

