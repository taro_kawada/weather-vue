const weatherAPI = "2873223f393938d27d9f625250bfb218";
// https://api.openweathermap.org/data/2.5/forecast?q=London,us&appid=2873223f393938d27d9f625250bfb218

var app = new Vue({
  el: '#app',
  data: {
    submit: 'Submit',
    showCards: false,
    showResult: false,
    error: false,
    places: [
      'Tokyo',
      'Yokohama',
      'Kyoto',
      'Osaka',
      'Sapporo',
      'Nagoya'
    ],
    search: "",
    placesData: [],
    placesObj: {},
    selectedPlace: null,
    errorMessage: "",
    selectedPlaceList: null,
    images: {
      Tokyo: "https://bit.ly/2QVYsr7",
      Nagoya: "https://www.japan-guide.com/g2/2155_01.jpg",
      Yokohama: "https://gaijinpot.scdn3.secure.raxcdn.com/app/uploads/sites/6/2016/06/Yokohama-Chinatown-1024x683.jpg",
      Kyoto: "https://kyoto.travel/assets/ui/main_onsen-06c64eaad26e53eecb274e1fbb285e5c.jpg",
      Osaka: "https://photos.smugmug.com/Osaka/Osaka-Categories/i-J9MFjBv/0/XL/Osaka_Districts-XL.jpg",
      Sapporo: "https://www.conventionsapporo.jp/img/about_sapporo.jpg",
    }
  },
  created: function () {
    // `this` points to the vm instance
    let places = this.places;
    for (var x = 0; x < places.length; x++) {
      axios.get(`https://api.openweathermap.org/data/2.5/forecast?q=${places[x]}&units=metric&appid=2873223f393938d27d9f625250bfb218`)
        .then(res => {
          this.placesData.push(res.data);
          this.placesObj[res.data.city.name] = res.data;
        })
        .catch(err => console.log(err))
    }
    this.showCards = true;
  },
  methods: {
    selectPlace: function (place) {

      $(document).ready(function(){
        $('.slick').slick({
          slidesToShow: 3,
          slidesToScroll: 3
        });
      });
      this.selectedPlace = this.placesObj[place];
      this.selectedPlaceList = this.placesObj[place].list;
      this.showResult = true;
      this.showCards = false;
    },
    back: function () {
      this.showResult = false;
      this.showCards = true;
    },
    lookup: function (event) {
      if (event) event.preventDefault();
      axios.get(`https://api.openweathermap.org/data/2.5/forecast?q=${this.search}&units=metric&appid=2873223f393938d27d9f625250bfb218`)
        .then(res => {
          this.placesObj['search'] = res.data;
          this.selectPlace('search');
        })
        .catch(() => {
          this.error = !this.error;
          this.errorMessage = `${this.search} is invalid`;
          setTimeout(function(){
            this.error = !this.error;
          }.bind(this), 2000)
        })
    }
  },
  mounted: {
    lookup: function() {
      console.log("asdas")
    }
  }
});

